data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["VPC_${upper(replace(var.name, "-", "_"))}"]
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.selected.id

  tags = {
    Tier = "Private"
  }
}

data "aws_subnet" "private" {
  for_each = data.aws_subnet_ids.private.ids
  id       = each.value
}

data "aws_lb" "external" {
  name = "${var.name}-external-lb"
}

data "aws_security_group" "external" {
  name = "${var.name}-external-lb-sg"
}

data "aws_lb" "internal" {
  name = "${var.name}-internal-lb"
}

data "aws_security_group" "internal" {
  name = "${var.name}-internal-lb-sg"
}

data "aws_route53_zone" "selected" {
  name = var.domain
}

data "aws_acm_certificate" "selected" {
  domain   = "*.${var.domain}"
  statuses = ["ISSUED"]
}
