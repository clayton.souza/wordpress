resource "aws_instance" "this" {
  ami                     = var.wordpress_bitnami_ami
  disable_api_termination = true
  instance_type           = var.instance_type
  key_name                = var.name
  security_groups         = [aws_security_group.this.id]
  subnet_id               = element(tolist(data.aws_subnet_ids.private.ids), 0)

  tags = {
    Name    = "${var.name}-wordpress"
    Product = var.name
  }

  volume_tags = {
    Name    = "${var.name}-wordpress-volume"
    Product = var.name
  }

  root_block_device {
    volume_type = "standard"
    volume_size = 50
  }
}

resource "aws_lb_target_group_attachment" "this" {
  target_group_arn = aws_lb_target_group.this.arn
  target_id        = aws_instance.this.id
  port             = 80
}