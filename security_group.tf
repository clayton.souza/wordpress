resource "aws_security_group" "this" {
  name   = "${var.name}-wordpress-sg"
  vpc_id = data.aws_vpc.selected.id

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "internet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "${var.name}-wordpress-sg"
  }
}

# Allow access from the load balancer
resource "aws_security_group_rule" "wordpress" {
  description              = "wordpress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  type                     = "ingress"
  security_group_id        = aws_security_group.this.id
  source_security_group_id = data.aws_security_group.external.id
}