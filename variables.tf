variable "domain" {
  description = "Application domain"
  type        = string
}

variable "instance_type" {
  description = "EC2 instance type"
  type        = string
}

variable "name" {
  description = "Product Name"
  type        = string
}

variable "pem" {
  description = "Pem name"
  type        = string
}

variable "region" {
  description = "AWS Region to deploy resources"
  type        = string
}

variable "wordpress_bitnami_ami" {
  description = "Wordpress ami - ohio by default"
  default     = "ami-05eaf0a127fdc9011"
}
